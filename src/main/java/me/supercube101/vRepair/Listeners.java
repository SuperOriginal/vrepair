package me.supercube101.vRepair;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Aaron.
 */
public class Listeners implements Listener{

    static Main maini;
    public Listeners(Main main){
        maini = main;
    }

    public static void injectPlayer(String s){
        if(maini.getConfig().get(s) == null){
            maini.getConfig().set(s, 0);
            maini.saveConfig();
        }
        Main.tokens.put(s, TokenUtils.getTokens(s));
    }

    public static void removePlayer(String s){
        maini.getConfig().set(s, TokenUtils.getTokens(s));
        maini.saveConfig();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){

        injectPlayer(e.getPlayer().getName());
    }

    @EventHandler
    public void onLevae(PlayerQuitEvent e){
        removePlayer(e.getPlayer().getName());
    }

    @EventHandler
    public void onKick(PlayerKickEvent e){
        removePlayer(e.getPlayer().getName());
    }

}
