package me.supercube101.vRepair.CommandModules.AdminCmds;

import me.supercube101.vRepair.CommandInfo;
import me.supercube101.vRepair.RepairCommand;
import me.supercube101.vRepair.TokenUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Give a player repair tokens.", aliases = {"give", "g"})
public class Give extends RepairCommand{
    @Override
    public void execute(CommandSender sender, String[] args) {

        if(args.length <= 1){
            sender.sendMessage(ChatColor.RED + "Usage: /rt give <player> <amt>");
            return;
        }

        try{
            if(TokenUtils.getTokens(args[0]) == -1){
                sender.sendMessage(ChatColor.RED + args[0] + " does not exist in the database.");
                return;
            }
            TokenUtils.add(args[0], Integer.parseInt(args[1]));
            sender.sendMessage(ChatColor.GRAY + "You have given " + args[0] + " " + ChatColor.AQUA + args[1] + ChatColor.GRAY + " repair token(s). They now have " + ChatColor.AQUA + TokenUtils.getTokens(args[0]) + ChatColor.GRAY + " repair token(s).");

            if(Bukkit.getServer().getPlayer(args[0]) != null) Bukkit.getServer().getPlayer(args[0]).sendMessage(ChatColor.GRAY + "You have been given " + ChatColor.AQUA + args[1] + ChatColor.GRAY + " repair token(s). You now have " + ChatColor.AQUA + TokenUtils.getTokens(args[0]) + ChatColor.GRAY + ".");
        }catch (NumberFormatException e){
            sender.sendMessage(ChatColor.RED + "Please enter a valid number.");
        }

    }
}
