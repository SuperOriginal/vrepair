package me.supercube101.vRepair.CommandModules.AdminCmds;

import me.supercube101.vRepair.CommandInfo;
import me.supercube101.vRepair.RepairCommand;
import me.supercube101.vRepair.TokenUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Set a player's token balance." , aliases = {"set", "s"})
public class Set extends RepairCommand{
    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length <= 1){
            sender.sendMessage(ChatColor.RED + "Usage: /rt set <player> <amt>");
            return;
        }

        try{
            if(TokenUtils.getTokens(args[0]) == -1){
                sender.sendMessage(ChatColor.RED + args[0] + " does not exist in the database.");
                return;
            }
            int oldtokens = TokenUtils.getTokens(args[0]);
            TokenUtils.set(args[0], Integer.parseInt(args[1]));
            sender.sendMessage(ChatColor.GRAY + "You have set " + args[0] + "'s repair token(s) to " + ChatColor.AQUA + args[1] + ". They previously had " + ChatColor.AQUA + oldtokens + ChatColor.GRAY + " repair token(s).");
            if(Bukkit.getServer().getPlayer(args[0]) != null) Bukkit.getServer().getPlayer(args[0]).sendMessage(ChatColor.GRAY + "Your repair token balance has been set to " + ChatColor.AQUA + args[1] + ChatColor.GRAY + ".");
        }catch (NumberFormatException e){
            sender.sendMessage(ChatColor.RED + "Please enter a valid number.");
        }

    }

}
