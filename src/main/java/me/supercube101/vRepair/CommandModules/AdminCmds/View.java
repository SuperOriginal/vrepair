package me.supercube101.vRepair.CommandModules.AdminCmds;

import me.supercube101.vRepair.CommandInfo;
import me.supercube101.vRepair.RepairCommand;
import me.supercube101.vRepair.TokenUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "View a player's token balance.", aliases = {"view", "v"})
public class View extends RepairCommand{
    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0){
            sender.sendMessage(ChatColor.GRAY + "You have " + ChatColor.AQUA + TokenUtils.getTokens(sender.getName()) + ChatColor.GRAY + " token(s).");
            return;
        }

            if(TokenUtils.getTokens(args[0]) == -1){
                sender.sendMessage(ChatColor.RED + "This player does not exist in the database.");
                return;
            }
            int oldtokens = TokenUtils.getTokens(args[0]);
            sender.sendMessage(ChatColor.GRAY + args[0] + " has " + ChatColor.AQUA + TokenUtils.getTokens(args[0]) + ChatColor.GRAY + " token(s).");
    }
}
