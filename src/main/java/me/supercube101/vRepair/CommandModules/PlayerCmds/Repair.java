package me.supercube101.vRepair.CommandModules.PlayerCmds;

import me.supercube101.vRepair.CommandInfo;
import me.supercube101.vRepair.Main;
import me.supercube101.vRepair.RepairCommand;
import me.supercube101.vRepair.TokenUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Repair your items!" , aliases = "repair")
public class Repair extends RepairCommand{

    @Override
    public void execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;

        if(p.getItemInHand().getDurability() <= 0){
            p.sendMessage(ChatColor.GRAY + "You are not holding an item that can be repaired!");
            return;
        }

        ItemStack item = p.getItemInHand();
        int cost = 1;
        for(Enchantment enchant : item.getEnchantments().keySet()){
            if(item.getEnchantmentLevel(enchant) == 0) continue;
            if(item.getEnchantmentLevel(enchant) < 6){
                cost = cost + 1;
                continue;
            }
            if(item.getEnchantmentLevel(enchant) < 11){
                cost = cost + 2;
                continue;
            }
            if(item.getEnchantmentLevel(enchant) < 16){
                cost = cost + 3;
                continue;
            }
                cost = cost + 4;
        }
        String name = p.getItemInHand().getType().toString().toLowerCase().replace('_', ' ');
        if(p.getItemInHand().getItemMeta().hasDisplayName()) name = p.getItemInHand().getItemMeta().getDisplayName();

        if(TokenUtils.getTokens(p.getName()) < cost){
            p.sendMessage(ChatColor.GRAY + "It costs " + ChatColor.AQUA + cost + ChatColor.GRAY + " repair tokens to repair this " + name + ", but you only have " + ChatColor.AQUA + TokenUtils.getTokens(p.getName()) + ChatColor.GRAY + ".");
            return;
        }


        p.sendMessage(ChatColor.GRAY + "You are about to repair " + name + " for " + ChatColor.AQUA + cost + ChatColor.GRAY + " tokens. Your token balance is " + ChatColor.AQUA + TokenUtils.getTokens(p.getName()) + ChatColor.GRAY + ". Please type /confirm to continue.");
        Main.confirming.put(p.getName(), cost);
    }
}
