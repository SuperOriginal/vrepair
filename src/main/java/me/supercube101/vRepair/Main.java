package me.supercube101.vRepair;

import me.supercube101.vRepair.CommandModules.AdminCmds.*;
import me.supercube101.vRepair.CommandModules.AdminCmds.Set;
import me.supercube101.vRepair.CommandModules.PlayerCmds.Repair;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * Created by Aaron.
 */
public class Main extends JavaPlugin{
    
    private ArrayList<RepairCommand> admincmds;
    private ArrayList<RepairCommand> playercmds;

    public static HashMap<String, Integer> tokens;

    public static HashMap<String, Integer> confirming = new HashMap<String, Integer>();
    public static TokenUtils util;
    
    public void onEnable(){
        admincmds = new ArrayList<RepairCommand>();
        playercmds = new ArrayList<RepairCommand>();
        admincmds.add(new Give());
        admincmds.add(new Set());
        admincmds.add(new View());
        playercmds.add(new Repair());

        tokens = new HashMap<String, Integer>();
        util = new TokenUtils(this);

        Bukkit.getServer().getPluginManager().registerEvents(new Listeners(this), this);

        for(Player p : Bukkit.getOnlinePlayers()){
            Listeners.injectPlayer(p.getName());
        }
    }

    public void onDisable(){
        for(Player p : Bukkit.getOnlinePlayers()){
            Listeners.removePlayer(p.getName());
        }
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandlbl, String[] args){
        
        handleAdminCommands(sender, cmd, args);
        handlePlayerCommands(sender,cmd,args);

        if(cmd.getName().equalsIgnoreCase("confirm")){
            if(!sender.hasPermission("vrepair.use")){
                sender.sendMessage(ChatColor.RED + "No permission");
                return true;
            }
            if(!(sender instanceof Player)) return true;
            Player p = (Player) sender;

            if(!confirming.containsKey(p.getName())){
                p.sendMessage(ChatColor.GRAY + "You do not have a pending repair.");
                return true;
            }

            p.getItemInHand().setDurability((short) 0);
            TokenUtils.set(p.getName(), TokenUtils.getTokens(p.getName()) - confirming.get(p.getName()));
            String name = p.getItemInHand().getType().toString().toLowerCase().replace('_',' ');
            if(p.getItemInHand().getItemMeta().hasDisplayName()) name = p.getItemInHand().getItemMeta().getDisplayName();
            p.sendMessage(ChatColor.GRAY + "You successfully repaired "  + name + " for " + ChatColor.AQUA + confirming.get(p.getName()) + ChatColor.GRAY + " repair tokens. You now have " + ChatColor.AQUA + TokenUtils.getTokens(p.getName()) + ChatColor.GRAY + " tokens.");
            confirming.remove(p.getName());
        }

        return true;
    }
    
    public void handleAdminCommands(CommandSender sender, Command cmd, String[] args){


        if(cmd.getName().equalsIgnoreCase("rt")){
            if(!sender.hasPermission("vrepair.admin")){
                sender.sendMessage(ChatColor.RED + "No permission.");
                return;
            }
            if(args.length == 0){
                for(RepairCommand pcmd : admincmds){
                    CommandInfo info = pcmd.getClass().getAnnotation(CommandInfo.class);
                    sender.sendMessage(ChatColor.GRAY + "/rt " + ChatColor.AQUA + info.aliases()[0] + ChatColor.GRAY + ": " + info.desc());
                }
                return;
            }

            RepairCommand requested = null;

            outer : for(RepairCommand pcmd : admincmds){
                CommandInfo info = pcmd.getClass().getAnnotation(CommandInfo.class);
                for(String alias : info.aliases()){
                    if(alias.equalsIgnoreCase(args[0])){
                        requested = pcmd;
                        break outer;
                    }
                }
            }

            if(requested == null){
                sender.sendMessage(ChatColor.RED + "Invalid args, /rt for help.");
                return;
            }

            List<String> newargs = new LinkedList<String>(Arrays.asList(args));
            newargs.remove(0);
            args = newargs.toArray(new String[newargs.size()]);

            requested.execute(sender, args);

        }
    }

    public void handlePlayerCommands(CommandSender sender, Command cmd, String[] args){
        if(cmd.getName().equalsIgnoreCase("repair")){
            if(!sender.hasPermission("vrepair.use")){
                sender.sendMessage(ChatColor.RED + "No permission.");
                return;
            }
            RepairCommand repair = new Repair();
            repair.execute(sender, args);
        }
    }



}
