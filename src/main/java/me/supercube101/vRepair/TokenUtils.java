package me.supercube101.vRepair;

import org.bukkit.Bukkit;

/**
 * Created by Aaron.
 */
public class TokenUtils {

    static Main kmain;

    public TokenUtils(Main main){
        kmain = main;
    }

    public static void add(String s, int amt){
        if(Bukkit.getServer().getPlayer(s) != null) {
            if (Main.tokens.containsKey(s)) {
                int before = Main.tokens.get(s);
                Main.tokens.put(s, before + amt);
            } else {
                Main.tokens.put(s, amt);
            }
        }else{
            kmain.getConfig().set(s, kmain.getConfig().getInt(s) + amt);
            kmain.saveConfig();
        }
    }

    public static void remove(String s, int amt){
        if(Bukkit.getServer().getPlayer(s) != null) {
            if (Main.tokens.containsKey(s)) {
                int before = Main.tokens.get(s);
                if (before - amt < 0) Main.tokens.put(s, 0);
                else Main.tokens.put(s, 0);
            } else {
                Main.tokens.put(s, 0);
            }
        }else{
            if(kmain.getConfig().getInt(s) - amt < 0) kmain.getConfig().set(s, 0);
            else kmain.getConfig().set(s, kmain.getConfig().getInt(s) + amt);
            kmain.saveConfig();
        }
    }

    public static void set(String s, int amt){
        if(Bukkit.getServer().getPlayer(s) != null)
            Main.tokens.put(s, amt);
        else
            kmain.getConfig().set(s, amt);
        kmain.saveConfig();
    }

    public static int getTokens(String s){
        int value;
        if(kmain.getConfig().get(s) == null && !Main.tokens.containsKey(s)){
            value = -1;
            return value;
        }
        if(Main.tokens.containsKey(s)) value = Main.tokens.get(s);
        else value = kmain.getConfig().getInt(s);

        return value;

    }


}
