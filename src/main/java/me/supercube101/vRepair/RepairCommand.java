package me.supercube101.vRepair;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
public abstract class RepairCommand {
    public abstract void execute(CommandSender sender, String[] args);
}
